<?php

namespace Flyshot\SharedComponentsBundle\Command;

use Flyshot\SharedComponentsBundle\Security\SDKTokenGenerator;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateJwtCommand extends Command
{
    protected static $defaultName = 'flyshot:generate-sdk-token';

    /**
     * @var SDKTokenGenerator
     */
    private $generator;

    public function setGenerator(SDKTokenGenerator $generator)
    {
        $this->generator = $generator;
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate SDK authorization token')
            ->addArgument('sdk-user-uuid', InputArgument::REQUIRED)
            ->addArgument('customer-uuid', InputArgument::OPTIONAL)
            ->addArgument('application-uuid', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $sdkUserUuid = $input->getArgument('sdk-user-uuid');
        $customerUuid = $input->getArgument('customer-uuid');
        $applicationUuid = $input->getArgument('application-uuid');

        $token = $this->generator->generate(
            Uuid::fromString($sdkUserUuid),
            $customerUuid ? Uuid::fromString($customerUuid) : null,
            $applicationUuid ? Uuid::fromString($applicationUuid): null
        );

        $io->writeln('Token generated and signed by application secret key');
        $io->writeln('Token:<info> '.$token.'</info>');
    }
}
