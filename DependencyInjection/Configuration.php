<?php

namespace Flyshot\SharedComponentsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('flyshot_components');
        $rootNode
            ->children()
            ->scalarNode('jwt_secret')
            ->isRequired()
            ->end();

        return $treeBuilder;
    }
}
