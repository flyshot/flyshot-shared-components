<?php

namespace Flyshot\SharedComponentsBundle\Security\User;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class JWTUser implements UserInterface, EquatableInterface
{
    private $uuid;
    private $payload;
    private $roles = [];

    public function __construct(UuidInterface $uuid, $payload = null)
    {
        $this->uuid = $uuid;
        $this->payload = $payload;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function getRoles()
    {
        return array_merge(['ROLE_USER'], $this->roles);
    }

    public function getPassword()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->uuid->toString();
    }

    public function eraseCredentials()
    {
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof self) {
            return false;
        }

        return $user->getUuid()->equals($this->uuid);
    }
}
