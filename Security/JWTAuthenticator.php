<?php

namespace Flyshot\SharedComponentsBundle\Security;

use Flyshot\ApiUtilsBundle\Response\APIResponseBuilder;
use Flyshot\ApiUtilsBundle\Response\Error;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class JWTAuthenticator extends AbstractGuardAuthenticator
{
    private $manager;

    private $logger;

    private $builder;

    public function __construct(JWTManager $manager, LoggerInterface $logger, APIResponseBuilder $builder)
    {
        $this->manager = $manager;
        $this->logger = $logger;
        $this->builder = $builder;
    }

    public function supports(Request $request)
    {
        return $request->headers->has('token');
    }

    public function getCredentials(Request $request)
    {
        return [
            'token' => $request->headers->get('token'),
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $jwt = $credentials['token'];

        if (null === $jwt) {
            return null;
        }

        try {
            $uuid = $this->manager->decode($jwt);
            $user = $userProvider->loadUserByUsername($uuid);
        } catch (\Throwable $e) {
            $this->logger->alert('JWT authenticator error: '.$e->getMessage());
            return null;
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $response = $this->builder->error(new Error('authentication_failure'))->getResponse();
        $response->setStatusCode(403);

        return $response;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $this->logger->alert('You are not authorized: '.$authException->getMessage());

        $response = $this->builder->error(new Error('authentication_required'))->getResponse();
        $response->setStatusCode(401);

        return $response;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
