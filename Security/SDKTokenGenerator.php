<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 23/08/2019
 * Time: 12:38
 */

namespace Flyshot\SharedComponentsBundle\Security;

use Ramsey\Uuid\UuidInterface;

class SDKTokenGenerator
{
    private $manager;

    public function __construct(JWTManager $manager)
    {
        $this->manager = $manager;
    }

    public function generate(
        UuidInterface $sdkUserUuid,
        ?UuidInterface $customerUuid = null,
        ?UuidInterface $applicationUuid = null
    ): string
    {
        $payload = [
            'customerUuid' => ($customerUuid) ? $customerUuid->toString() : null,
            'applicationUuid' => ($applicationUuid) ? $applicationUuid->toString() : null,
        ];

        return $this->manager->encode($sdkUserUuid, $payload);
    }
}
