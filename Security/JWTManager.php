<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 20/01/2019
 * Time: 16:22
 */

namespace Flyshot\SharedComponentsBundle\Security;

use Firebase\JWT\JWT;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class JWTManager
{
    private $secret;

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }

    public function encode(UuidInterface $uuid, array $payload = []): string
    {
        $payload['uuid'] = $uuid->toString();

        return JWT::encode($payload, $this->secret);
    }

    public function decode(string $token, &$payload = null): UuidInterface
    {
        $payload = JWT::decode($token, $this->secret, ['HS256']);

        return Uuid::fromString($payload->uuid);
    }
}